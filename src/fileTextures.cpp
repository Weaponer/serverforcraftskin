#include"fileTexture.h"

InfoTextures ManagerTexture::infoFile;


int ManagerTexture::managerTextureInit(){
    int createDir = mkdir(NAME_DIRECTORY_TEXTURE,S_IRWXU);

    if(createDir == -1){
        infoFile.file = fopen("./" NAME_DIRECTORY_TEXTURE "/" NAME_INFOFILE_TEXTURE, "rb");

        if(infoFile.file == NULL){
            infoFile.file = fopen("./" NAME_DIRECTORY_TEXTURE "/" NAME_INFOFILE_TEXTURE, "wb");
            if(infoFile.file == NULL){
                return -1;
            }
            infoFile.countTextures = 0;
            infoFile.endUpdateNum = 0;
            fwrite(&(infoFile.endUpdateNum),sizeof(infoFile.endUpdateNum),1,infoFile.file);
            fwrite(&(infoFile.countTextures),sizeof(infoFile.countTextures),1,infoFile.file);
        }else{
            fread(&(infoFile.endUpdateNum),sizeof(infoFile.endUpdateNum),1,infoFile.file);
            fread(&(infoFile.countTextures),sizeof(infoFile.countTextures),1,infoFile.file);
        }
    }else{
            infoFile.file = fopen( "./" NAME_DIRECTORY_TEXTURE "/" NAME_INFOFILE_TEXTURE, "wb");
            if(infoFile.file == NULL){
                    return -2;
                }
            infoFile.countTextures = 0;
            infoFile.endUpdateNum = 0;
            fwrite(&(infoFile.endUpdateNum),sizeof(infoFile.endUpdateNum),1,infoFile.file);
            fwrite(&(infoFile.countTextures),sizeof(infoFile.countTextures),1,infoFile.file);
    }
    if(infoFile.countTextures > 0){   //if the number of news is greater than zero, then the file already existed
        infoFile.vectorTextures.reserve(infoFile.countTextures);

        for(int i = 0; i < infoFile.countTextures;i++){
            int countChar = 0;
            fread(&countChar,sizeof(countChar),1,infoFile.file);
            char *chars = new char[countChar + 1];
            chars[countChar] = '\0';
        
            fread(chars, sizeof(char), countChar,infoFile.file);
            infoFile.vectorTextures.push_back(chars);

            delete[] chars;       
        }
        std::reverse(infoFile.vectorTextures.begin(), infoFile.vectorTextures.end());
    }
    fclose(infoFile.file);
    return 0;
}

int ManagerTexture::getCountTexture(){
    return infoFile.countTextures;
}

int ManagerTexture::getNumUpdate(){
    return infoFile.endUpdateNum;
}

int ManagerTexture::addTexture(Texture *texture){
    std::string str(texture->nameTexture);
    str.erase(std::remove(str.begin(),str.end(),'\n'),str.end());
    str.erase(std::remove(str.begin(),str.end(),' '),str.end());
    

    //The check on no copy
    std::vector<std::string>::iterator findTest = std::find(infoFile.vectorTextures.begin(), infoFile.vectorTextures.end(), str);
    if(findTest != infoFile.vectorTextures.end()){
        
        str.append(std::to_string(infoFile.countTextures));
        findTest = std::find(infoFile.vectorTextures.begin(), infoFile.vectorTextures.end(), str);
        if(findTest != infoFile.vectorTextures.end()){
            return -1;
        }
    }
    //


    std::string strPath("./" NAME_DIRECTORY_TEXTURE "/");
    strPath.append(str.c_str());
    FILE *fileNews = fopen(strPath.c_str(), "wb");
    
    if( fileNews != NULL && 0 == writeFileTexture(fileNews,texture)){
        infoFile.vectorTextures.push_back(str);
        infoFile.countTextures++;
        infoFile.addEndUpdateNum();
        return writeInfoFile();
    }else{
        return -1;
    }
}


int ManagerTexture::editTexture(int num, Texture *texture){
    if(infoFile.countTextures > num && texture != NULL){
        std::string strPath("./" NAME_DIRECTORY_TEXTURE "/");
        strPath.append(infoFile.vectorTextures[num]);
        FILE *fileNews = fopen(strPath.c_str(), "wb");       
        if(fileNews == NULL){
            return -1;
        }else{
            if(writeFileTexture(fileNews, texture) == 0){
                infoFile.addEndUpdateNum();
                return writeInfoFile();
            }else{
                return -1;
            }
        }
    }else{
        return -1;
    }
}

int ManagerTexture::writeFileTexture(FILE *file, Texture *texture){
    int countWrite = 0;
    countWrite = fwrite(&(texture->countPixel),sizeof(texture->countPixel),1,file);
    if(countWrite < 1){
        fclose(file);
        return 0;
    }
    countWrite = 0;
    for(int i = 0; i < texture->countPixel; i++){
        countWrite += fwrite(&(texture->pixels[i].r),sizeof(char),1,file);
        countWrite += fwrite(&(texture->pixels[i].g),sizeof(char),1,file);
        countWrite += fwrite(&(texture->pixels[i].b),sizeof(char),1,file);
        countWrite += fwrite(&(texture->pixels[i].a),sizeof(char),1,file);
    }
    if(countWrite == texture->countPixel * 4){
        fclose(file);
        return 0;
    }

    fclose(file);
    return 0;
}

Texture* ManagerTexture::getTexture(int num){
    if(num < infoFile.countTextures){     
        std::string str("./" NAME_INFOFILE_TEXTURE "/");
        str.append(infoFile.vectorTextures[num]);     
        FILE *file = fopen(str.c_str(), "rb");
        if(file != NULL){   
            
            int countPixel = 0;
            int countRead = fread(&countPixel, sizeof(int),1,file);
            if(countRead != 1 || countPixel != (SIZE_TEXTURE * SIZE_TEXTURE)){
                fclose(file);
                return NULL;
            }
            countRead = 0;
            Texture* texture = new Texture(SIZE_TEXTURE,SIZE_TEXTURE);
            for(int i = 0; i < countPixel;i++){
                countRead += fread(&(texture->pixels[i].r), sizeof(char),1,file);
                countRead += fread(&(texture->pixels[i].g), sizeof(char),1,file);
                countRead += fread(&(texture->pixels[i].b), sizeof(char),1,file);
                countRead += fread(&(texture->pixels[i].a), sizeof(char),1,file);
            }
            texture->nameTexture = infoFile.vectorTextures[num];
            if( countRead < texture->countPixel * 4){
                delete texture;
                fclose(file);
                return NULL;
            }else{
                fclose(file);
                return texture;
            }
        }else{
            return NULL;
        }
    }else{
        return NULL;
    }
}

int ManagerTexture::removeTexture(int num){
    if(infoFile.countTextures > num){
        std::string strPath("./" NAME_DIRECTORY_TEXTURE "/");
        strPath.append(infoFile.vectorTextures[num]);
        if( 0 == remove(strPath.c_str()) ){
            infoFile.vectorTextures.erase(infoFile.vectorTextures.begin() + num);
            infoFile.countTextures--;
            infoFile.addEndUpdateNum();
            return writeInfoFile();
        }else{
            return -1;
        }
    }else{
        return -1;
    }
}


int ManagerTexture::writeInfoFile(){
    infoFile.file = fopen("./" NAME_DIRECTORY_TEXTURE "/" NAME_INFOFILE_TEXTURE, "wb");
    if(infoFile.file != NULL){
        int countWrite = 0;

        countWrite = fwrite(&(infoFile.countTextures),sizeof(infoFile.countTextures),1,infoFile.file);
        if(countWrite < 1){
            fclose(infoFile.file);
            return -1;
        }

        for(int i = 0; i < infoFile.countTextures; i++){

            int countChar = infoFile.vectorTextures[i].length();
            const char *c_str = infoFile.vectorTextures[i].c_str();

            countWrite = fwrite(&infoFile.endUpdateNum,sizeof(infoFile.endUpdateNum),1,infoFile.file);
            countWrite += fwrite(&countChar,sizeof(countChar),1,infoFile.file);

            if(countWrite < 2){
                fclose(infoFile.file);
                return -1;
            }
            
            countWrite = 0;
            for(int i = 0; i < countChar;i++){
                countWrite += fwrite(c_str + i,sizeof(char),1,infoFile.file);
            }
            if(countWrite < countChar){
                fclose(infoFile.file);
                return -1;
            }
        }
        fclose(infoFile.file);
    }else{
        return -1;
    }
    return 0;
}

int ManagerTexture::managerTexture_Close(){
    return 0;
}






