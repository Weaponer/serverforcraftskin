#include"logRegister.h"

FILE *LogRegister::writeFile;

int LogRegister::initNewProcess(){
    if(writeFile == NULL){
        mkdir(DERICTORY_NAME, S_IRWXU);

        time_t timer;
        timer = time(NULL);
        const char *tm = ctime(&timer);

        std::string str;
        str.append("./ " DERICTORY_NAME "/Log");
        str.append(tm);
        str.erase(std::remove(str.begin(),str.end(),' '),str.end());

        writeFile = fopen(str.c_str(), "w");

        if(writeFile != NULL){
            writeLog("Load process");
            return 0;
        }else{
            return -1;
        }
    }else{
        return -1;
    }
}


int LogRegister::writeLog(const char *str_c){
    if(writeFile != NULL){ 
        std::string str("LOG - ");
        time_t timer = time(NULL);
        str.append(ctime(&timer));
        str.erase(std::remove(str.begin(),str.end(),'\n'),str.end());
        str.append(": ");
        str.append(str_c);
        str.append("\n");

        fprintf(writeFile,str.c_str(),NULL);
        fflush(writeFile);
        return 0;
    }else{  
        return -1;
    }
}

int LogRegister::writeWarning(const char *str_c){
    if(writeFile != NULL){ 
        std::string str("WARNING - ");
        time_t timer = time(NULL);
        str.append(ctime(&timer));
        str.erase(std::remove(str.begin(),str.end(),'\n'),str.end());
        str.append(": ");
        str.append(str_c);
        str.append("\n");

        fprintf(writeFile,str.c_str(),NULL);
        fflush(writeFile);
        return 0;
    }else{  
        return -1;
    }
}

int LogRegister::writeError(const char *str_c){
    if(writeFile != NULL){ 
        std::string str("ERROR - ");
        time_t timer = time(NULL);
        str.append(ctime(&timer));
        str.erase(std::remove(str.begin(),str.end(),'\n'),str.end());
        str.append(": ");
        str.append(str_c);
        str.append("\n");
        
        fprintf(writeFile,str.c_str(),NULL);
        fflush(writeFile);
        return 0;
    }else{  
        return -1;
    }
}

int LogRegister::closeProcess(){
    if(writeFile == NULL){
        return -1;
    }else{
        writeLog("Close process");
        return fclose(writeFile);
    }
}