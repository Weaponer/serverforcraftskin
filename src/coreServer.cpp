#include"coreServer.h"
#include"logRegister.h"
#include"workClient.h"
#include<stdlib.h>
#include<errno.h>

//Info base socket
int ServerSocketInfo::baseSocketDescriptor = -1;

std::list<Client*> ServerSocketInfo::clients;

//Base socket
int BaseSocket::startSocket(int countListen){
    int sock;
    int opt = 1;
    sockaddr_in addr;

    sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(sock < 0){
        std::string str("Socket not create: ");
        str.append(strerror(errno));
        LogRegister::writeError(str.c_str());
        return -1;
    }

    fcntl(sock, F_SETFL,fcntl(sock, F_GETFL, 0) | O_NONBLOCK);

    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT_SERVER);
    addr.sin_addr.s_addr = inet_addr(IP_SERVER);

    setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,&opt,sizeof(opt));

    if(bind(sock,  (struct sockaddr *)&addr, sizeof(addr))<0){
        std::string str("Socket not bind: ");
        str.append(strerror(errno));
        LogRegister::writeError(str.c_str());
        return -1;
    }

    listen(sock, countListen);

    ServerSocketInfo::baseSocketDescriptor = sock;
    LogRegister::writeLog("Create base socket.");
    return 0;
}

int BaseSocket::closeSocket(){
    close(ServerSocketInfo::baseSocketDescriptor);
    ServerSocketInfo::baseSocketDescriptor = -1;
    return 0;
}


//Update select
int UpdateSelect::runUpdate(){
        fd_set readset;
        FD_ZERO(&readset);
        FD_SET(ServerSocketInfo::baseSocketDescriptor,&readset);

        for(auto it = ServerSocketInfo::clients.begin(); it != ServerSocketInfo::clients.end(); it++){
            FD_SET((*it)->descriptor, &readset);
        }

        timeval timeout;
        timeout.tv_sec = 0;
        timeout.tv_usec = 220;//millisecond timeout

        int mx = ServerSocketInfo::baseSocketDescriptor;
        for(auto tp = ServerSocketInfo::clients.begin(); tp != ServerSocketInfo::clients.end(); ++tp ){
            if((*tp)->descriptor > mx) mx = (*tp)->descriptor;
        }

        if(select(mx + 1, &readset, NULL, NULL, &timeout) < 0){
            
        }

        if(FD_ISSET(ServerSocketInfo::baseSocketDescriptor, &readset)){
            ConnectClient::connectClient(ServerSocketInfo::baseSocketDescriptor);
        }

        for(auto it = ServerSocketInfo::clients.begin(); it != ServerSocketInfo::clients.end();){
            auto t = it;
            ++it;
            int readBase = 0;
            if(FD_ISSET((*t)->descriptor,&readset)){
               readBase = ResolverClient::resolveClient(*t);
            }
            
            if(readBase < 0){
                ConnectClient::disconnectClient(*t);
                continue;
            }

            if((*t)->paketWork != NULL && (*t)->paketWork->isEnd){
                (*t)->destroyPacket();
            }else if((*t)->paketWork != NULL){
                (*t)->paketWork->update();
            }else{                
                if(ResolverClient::checkTimeClient(*t)){
                    LogRegister::writeLog("End time");
                    ResolverClient::sendDisconnectClint(*t);
                    ConnectClient::disconnectClient(*t);
                }
            }
        }
    return 0;
}


//Connect client
char *get_ip_str(const struct sockaddr *sa, char *s, size_t maxlen)
{
    switch(sa->sa_family) {
        case AF_INET:
            inet_ntop(AF_INET, &(((struct sockaddr_in *)sa)->sin_addr),
                    s, maxlen);
            break;
        case AF_INET6:
            inet_ntop(AF_INET6, &(((struct sockaddr_in6 *)sa)->sin6_addr),
                    s, maxlen);
            break;
        default:
            return NULL;
    }
    return s;
}

int ConnectClient::connectClient(int sock){
    sockaddr_in addr;
    int size = sizeof(sockaddr);

    int client = accept(sock, (struct sockaddr *)&addr, (socklen_t *)&size);
    if(client < 0){
        LogRegister::writeWarning("Client could not connect.");
        return -1;
    }

    fcntl(client,F_SETFL, fcntl(client, F_GETFL, 0) | O_NONBLOCK );

    //Create new client struct
    Client *struc_client = new Client();

    struc_client->status = NoResolve;
    struc_client->timeArrivalMessage = time(NULL);
    struc_client->descriptor = client;
    struc_client->addr = addr;

    ServerSocketInfo::clients.push_front(struc_client);

    //Log
    std::string str;
    char c_str[16];

    get_ip_str((struct sockaddr *)&addr, c_str, 16);

    str.append("Client ");
    str.append(c_str);
    str.append(" could connect.");

    LogRegister::writeLog(str.c_str());
    return 0;
}

int ConnectClient::disconnectClient(Client *client){
    //Remove packet
    if(client->paketWork != NULL){
        client->destroyPacket();
    }
    
    //Remove client
    close(client->descriptor);
    ServerSocketInfo::clients.remove(client);
    delete client;
    
    //Log
    std::string str;
    str.append( "Client disconnect. count clietn: ");
    str.append(std::to_string(ServerSocketInfo::clients.size()));
    LogRegister::writeLog(str.c_str());
    return 0;
}

void Client::destroyPacket(){
    LogRegister::writeLog("Client destroy packet");
    delete paketWork;
    paketWork = NULL;
}