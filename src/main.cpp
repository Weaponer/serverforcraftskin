#include"logRegister.h"
#include"coreServer.h"
#include"fileNews.h"
#include"fileTexture.h"
#include<sys/types.h>
#include<sys/stat.h>
#include<iostream>
#include<cstring>
#include <sys/file.h>
#include <execinfo.h>
#include <unistd.h>
#include <errno.h>
#include <wait.h>


//#define DEBUG_BUILD

#define NAME_FILE_PID "./pidDemon"
#define CD_DIR "/root/server/"

#define CHILD_NEED_WORK         1
#define CHILD_NEED_TERMINATE    2


int monitorDemon();

int workDemon();

int loadProgram(){
    LogRegister::initNewProcess();
    if(ManagerNews::managerNews_Init() < 0){
        return CHILD_NEED_TERMINATE;
    }
    if(ManagerTexture::managerTextureInit() < 0){
        return CHILD_NEED_TERMINATE;
    }

   /* if(ManagerNews::getCountNews() == 0){
        News news;
        news.titleEn = "EnTitle";
        news.contentEn = "EnContent";
        news.titleRu = "RuTitle";
        news.contentRu = "RuContent";
        ManagerNews::addNews(&news);
    }*/

    /*if(ManagerTexture::getCountTexture() == 0){
        Texture texture(SIZE_TEXTURE,SIZE_TEXTURE);
        texture.nameTexture = "testTexture";
        ManagerTexture::addTexture(&texture);
    }*/

    if(BaseSocket::startSocket(2) < 0){
        return CHILD_NEED_TERMINATE;
    }

    return CHILD_NEED_WORK;
}

int closeProgram(){
    BaseSocket::closeSocket();
    ManagerNews::managerNews_Close();
    ManagerTexture::managerTexture_Close();
    LogRegister::closeProcess();
    return 0;
}

#ifdef DEBUG_BUILD
volatile bool isExit;

void exitProces(int signal){
    isExit = true;
}
#endif

int main(){
#ifdef DEBUG_BUILD
    if(loadProgram()==CHILD_NEED_TERMINATE){
        return 1;
    }

    signal(SIGINT, exitProces);
    while (!isExit)
    {
        UpdateSelect::runUpdate();
    }
    LogRegister::writeLog("End update select server.");

    closeProgram();
    return 0;
#else
    int pid;
    
    pid = fork();

    if(pid == -1){
        perror("Cant create demon.");
        return -1;
    }else if(pid == 0){
        umask(0);

        setsid();
        chdir(CD_DIR);

        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);

        return monitorDemon();
    }
#endif
}

void setPidFile(const char* Filename){
    FILE* f;
    f = fopen(Filename, "w+");
    if (f){
        fprintf(f, "%u", getpid());
        fclose(f);
    }
}


volatile int isClose;

static std::string strSignalClose;

void takeSignal(int signal){
    isClose = 1;
    strSignalClose.append(strsignal(signal));
}

int workDemon(){

    signal(SIGFPE, takeSignal);
    signal(SIGILL, takeSignal);
    signal(SIGSEGV, takeSignal);
    signal(SIGBUS, takeSignal);

    signal(SIGQUIT, takeSignal);
    signal(SIGINT, takeSignal);
    signal(SIGTERM, takeSignal);
    signal(SIGUSR1, takeSignal);

    sleep(60);

    if(loadProgram()==CHILD_NEED_TERMINATE){
        return CHILD_NEED_WORK;
    }

    while (!isClose)
    {
        UpdateSelect::runUpdate();
    }
    
    LogRegister::writeLog(strSignalClose.c_str());
    LogRegister::writeLog("End update select server.");

    closeProgram();

    return CHILD_NEED_WORK;
}

int monitorDemon(){
    int pid;
    int status;
    int need_start = 1;

    sigset_t sigset;
    siginfo_t siginfo;

    sigemptyset(&sigset);
    sigaddset(&sigset, SIGQUIT);
    sigaddset(&sigset, SIGINT);
    sigaddset(&sigset, SIGTERM);
    sigaddset(&sigset, SIGCHLD);

    sigaddset(&sigset, SIGUSR1);
    sigprocmask(SIG_BLOCK, &sigset, NULL);

    setPidFile(NAME_FILE_PID);

    while (true)
    {
        if(need_start){
            pid = fork();
        }

        need_start = 1;

        if(pid == -1){

        }else if(pid == 0){
            status = workDemon();

            exit(status);
        }else{
            sigwaitinfo(&sigset, &siginfo);

            if(siginfo.si_signo == SIGCHLD){
                wait(&status);
                status = WEXITSTATUS(status);

                if(status == CHILD_NEED_TERMINATE){
                    break;
                }else if(status == CHILD_NEED_WORK){

                }
            }else if(siginfo.si_signo == SIGUSR1){
                kill(pid, SIGUSR1);
                need_start = 0;
            }else{
                kill(pid, SIGTERM);
                status = 1;
                break;
            }
        }
    }
    
    unlink(NAME_FILE_PID);
    return status;
}