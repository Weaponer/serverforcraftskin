#include"fileNews.h"

InfoFileNews ManagerNews::infoFile;

int ManagerNews::managerNews_Init(){
    int createDir = mkdir(NAME_DIRECTORY_NEWS,S_IRWXU);

    if(createDir == -1){
        infoFile.file = fopen("./" NAME_DIRECTORY_NEWS "/" NAME_INFOFILE_NEWS, "r+b");

        if(infoFile.file == NULL){
            infoFile.file = fopen("./" NAME_DIRECTORY_NEWS "/" NAME_INFOFILE_NEWS, "wb");
            if(infoFile.file == NULL){
                return -1;
            }
            infoFile.countNews = 0;
            infoFile.endUpdateNum = 0;
            fwrite(&(infoFile.endUpdateNum),sizeof(infoFile.endUpdateNum),1,infoFile.file);
            fwrite(&(infoFile.countNews),sizeof(infoFile.countNews),1,infoFile.file);
        }else{
            fread(&(infoFile.endUpdateNum),sizeof(infoFile.endUpdateNum),1,infoFile.file);
            fread(&(infoFile.countNews),sizeof(infoFile.countNews),1,infoFile.file);
        }
    }else{
        infoFile.file = fopen( "./" NAME_DIRECTORY_NEWS "/" NAME_INFOFILE_NEWS, "wb");
        if(infoFile.file == NULL){
                return -2;
            }
        infoFile.countNews = 0;
        infoFile.endUpdateNum = 0;
        fwrite(&(infoFile.endUpdateNum),sizeof(infoFile.endUpdateNum),1,infoFile.file);
        fwrite(&(infoFile.countNews),sizeof(infoFile.countNews),1,infoFile.file);
    }

    if(infoFile.countNews > 0){   //if the number of news is greater than zero, then the file already existed
        infoFile.vectorNews.reserve(infoFile.countNews);

        for(int i = 0; i < infoFile.countNews;i++){
            int countChar = 0;
            fread(&countChar,sizeof(countChar),1,infoFile.file);
            char *chars = new char[countChar + 1];
            chars[countChar] = '\0';
    
            fread(chars, sizeof(char), countChar,infoFile.file);
            infoFile.vectorNews.push_back(chars);

            delete[] chars;       
        }
        std::reverse(infoFile.vectorNews.begin(), infoFile.vectorNews.end());
    }

    fclose(infoFile.file);
    return 0;
}

int ManagerNews::getCountNews(){
    return infoFile.countNews;
}

int ManagerNews::getNumUpdate(){
    return infoFile.endUpdateNum;
}

int ManagerNews::addNews(News *news){
    time_t timer; //The name to file a from current time 
    timer = time(NULL);

    std::string str;
    str.append(ctime(&timer));
    str.erase(std::remove(str.begin(),str.end(),'\n'),str.end());
    str.erase(std::remove(str.begin(),str.end(),' '),str.end());

    //The check on no copy
    std::vector<std::string>::iterator findTest = std::find(infoFile.vectorNews.begin(), infoFile.vectorNews.end(), str);
    if(findTest != infoFile.vectorNews.end()){
        
        str.append(std::to_string(infoFile.countNews));
        findTest = std::find(infoFile.vectorNews.begin(), infoFile.vectorNews.end(), str);
        if(findTest != infoFile.vectorNews.end()){
            return -1;
        }
    }
    //
    std::string strPath("./" NAME_DIRECTORY_NEWS "/");
    strPath.append(str.c_str());
    FILE *fileNews = fopen(strPath.c_str(), "wb");

    if(0 == writeFileNews(fileNews, news)){
        infoFile.vectorNews.push_back(str);
        infoFile.countNews++;
        infoFile.addEndUpdateNum();
        return writeInfoFile();
    }else{
        return -1;
    }
}

int ManagerNews::editNews(int num, News *news){
    if(infoFile.countNews > num && news != NULL){
        std::string strPath("./" NAME_DIRECTORY_NEWS "/");
        strPath.append(infoFile.vectorNews[num]);
        FILE *fileNews = fopen(strPath.c_str(), "wb");       
        if(fileNews == NULL){
            return -1;
        }else{
            if( writeFileNews(fileNews, news) == 0){
                infoFile.addEndUpdateNum();
                return writeInfoFile();
            }else{
                return -1;
            }
        }
    }else{
        return -1;
    }
}

int ManagerNews::removeNews(int num){
    if(infoFile.countNews > num){
        std::string strPath("./" NAME_DIRECTORY_NEWS "/");
        strPath.append(infoFile.vectorNews[num]);
        if( 0 == remove(strPath.c_str()) ){
            infoFile.vectorNews.erase(infoFile.vectorNews.begin() + num);
            infoFile.countNews--;
            infoFile.addEndUpdateNum();
            return writeInfoFile();
        }else{
            return -1;
        }
    }else{
        return -1;
    }
}

int ManagerNews::writeFileNews(FILE *file, News *news){  
    for(auto i = news->EnTitle.begin(); i != news->EnTitle.end(); i++)
        fwrite(&(*i),sizeof(char),1, file);
    for(auto i = news->EnContent.begin(); i != news->EnContent.end(); i++)
        fwrite(&(*i),sizeof(char),1, file);
    for(auto i = news->RuTitle.begin(); i != news->RuTitle.end(); i++)
        fwrite(&(*i),sizeof(char),1, file);
    for(auto i = news->RuContent.begin(); i != news->RuContent.end(); i++)
        fwrite(&(*i),sizeof(char),1, file);

    fclose(file);
    return 0;
}

void readLineFile(std::list<char> *listChar, FILE *file){
    char readChar;
    int countRead;
    while(true){
        countRead = fread(&readChar, sizeof(char), 1, file);
        listChar->push_back(readChar);
        if(readChar == '\0'){
            break;
        }
    }
}

News* ManagerNews::getNews(int num){
    if(num < infoFile.countNews){
        std::string str("./" NAME_DIRECTORY_NEWS "/");
        str.append(infoFile.vectorNews[num]);
        FILE *file = fopen(str.c_str(), "rb");

        if(file != NULL){
            News *news = new News;
    
            readLineFile(&(news->EnTitle),file);
            readLineFile(&(news->EnContent),file);
            readLineFile(&(news->RuTitle),file);
            readLineFile(&(news->RuContent),file);

            fclose(file);
            return news;
        }else{
            return NULL;
        }
    }else{
        return NULL;
    }
}

int ManagerNews::writeInfoFile(){
    infoFile.file = fopen("./" NAME_DIRECTORY_NEWS "/" NAME_INFOFILE_NEWS, "wb");
    if(infoFile.file != NULL){
        int countWrite = 0;

        countWrite = fwrite(&(infoFile.countNews),sizeof(infoFile.countNews),1,infoFile.file);
        if(countWrite < 1){
            fclose(infoFile.file);
            return -1;
        }

        for(int i = 0; i < infoFile.countNews; i++){

            int countChar = infoFile.vectorNews[i].length();
            const char *c_str = infoFile.vectorNews[i].c_str();

            countWrite = fwrite(&infoFile.endUpdateNum,sizeof(infoFile.endUpdateNum),1,infoFile.file);
            countWrite += fwrite(&countChar,sizeof(countChar),1,infoFile.file);

            if(countWrite < 2){
                fclose(infoFile.file);
                return -1;
            }
            
            countWrite = 0;
            for(int i = 0; i < countChar;i++){
                countWrite += fwrite(c_str + i,sizeof(char),1,infoFile.file);
            }
            if(countWrite < countChar){
                fclose(infoFile.file);
                return -1;
            }
        }
        fclose(infoFile.file);
    }else{
        return -1;
    }
    return 0;
}

int ManagerNews::managerNews_Close(){
    return 0;
}