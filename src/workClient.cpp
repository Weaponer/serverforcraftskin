#include"logRegister.h"
#include"workClient.h"


//Resolver client
int ResolverClient::resolveClient(Client *client){
    int byte_read = 0;
    char buff[1];
    
    client->timeArrivalMessage = time(NULL);

    byte_read = recv(client->descriptor, buff, 1, MSG_PEEK); // Test client connect
    if(byte_read == 0){
        return -1;
    }

    //Init to client after connect 
    if(client->status == NoResolve){
        byte_read = recv(client->descriptor,buff,1,0);
        if(byte_read == 1){
            client->status = (ClientStatus)buff[0];
        }else{
            return 0;
        }
    }

    //Create packet work
    if(client->paketWork == NULL){
        char buff2[5];// 1 - is char code to work, 4 char it is int
        byte_read = recv(client->descriptor, buff2,5,MSG_PEEK);
        if(byte_read != 5){
            return 0; //If no message to work
        }else{//Create packet
        byte_read = recv(client->descriptor, buff2, 5, 0);
            int num;
            std::memcpy(&num,buff2 + 1,sizeof(num));

            //Create packet
            if(client->status >= StandartClient){
                if(buff2[0] == GET_NEWS){
                    LogRegister::writeLog("Get news");
                    client->paketWork = new PacketGetNews(num, client->descriptor);
                }else if(buff2[0] == GET_COUNT_NEWS){
                    LogRegister::writeLog("Get count news");
                    client->paketWork = new PacketGetCountNews(client->descriptor);
                }else if(buff2[0] == GET_COUNT_TEXTURE){
                    LogRegister::writeLog("Get count texture");
                    client->paketWork = new PacketGetCountTexture(client->descriptor);
                }else if(buff2[0] == GET_TEXTURE){
                    LogRegister::writeLog("Get texture");
                    client->paketWork = new PacketGetTexture(num,client->descriptor);
                }else if(buff2[0] == GET_END_UPDATE_NEWS){
                    LogRegister::writeLog("Get num update news");
                    client->paketWork = new PacketEndUpdateNews(client->descriptor);
                }else if(buff2[0] == GET_END_UPDATE_TEXTURE){
                    LogRegister::writeLog("Get num update texture");
                    client->paketWork = new PacketEndUpdateTexture(client->descriptor);
                }else if(buff2[0] == GET_NAME_TEXTURE){
                    LogRegister::writeLog("Get name texture");
                    client->paketWork = new PacketGetNameTexture(num,client->descriptor);
                }
            }
            if(client->status >= AdminClient){
                if(buff2[0] == REMOVE_NEWS){
                    LogRegister::writeLog("Remove news");
                    client->paketWork = new PacketRemoveNews(num, client->descriptor);
                }else if(buff2[0] == SET_NEWS){
                    LogRegister::writeLog("Add news");
                    client->paketWork = new PacketAddNews(client->descriptor);
                }else if(buff2[0] == EDIT_NEWS){
                    LogRegister::writeLog("Edit news");
                    client->paketWork = new PacketEditNews(num, client->descriptor);
                }else if(buff2[0] == REMOVE_TEXTURE){
                    LogRegister::writeLog("Remove texture");
                    client->paketWork = new PacketRemoveTexture(num, client->descriptor);
                }else if(buff2[0] == SET_TEXTURE){
                    LogRegister::writeLog("Add texture");
                    client->paketWork = new PacketAddTexture(client->descriptor);
                }else if(buff2[0] == EDIT_TEXTURE){
                    LogRegister::writeLog("Edit texture");
                    client->paketWork = new PacketEditTexture(num, client->descriptor);
                }
            }
        }
    }
    return 0;
}

int ResolverClient::checkTimeClient(Client *client){
    time_t curTime = time(NULL);
    double dist_time = difftime(curTime, client->timeArrivalMessage);
    if(dist_time > MAX_TIME_DELAY){
        return 1;
    }
    return 0;
}

int ResolverClient::sendDisconnectClint(Client *client){
    char buff[1];
    buff[0] = CODE_DISCONECT_CLIENT;
    return send(client->descriptor,buff, 1,0);
}


void PacketGetNews::callUpdate(){//Send news from number
    printf("%i \n", numNews);
    printf("%i \n", ManagerNews::getCountNews());
    if(numStage == 0){
        News *newsLoad = ManagerNews::getNews(numNews);
        if(newsLoad != NULL){
            int countByte = newsLoad->EnTitle.size();
            countByte += newsLoad->EnContent.size();
            countByte += newsLoad->RuTitle.size();
            countByte += newsLoad->RuContent.size();
            countByte++;

            news.reserve(countByte);

            for(auto i = newsLoad->EnTitle.begin(); i != newsLoad->EnTitle.end(); i++){
                news.push_back(*i);
            }
            for(auto i = newsLoad->EnContent.begin(); i != newsLoad->EnContent.end(); i++){
                news.push_back(*i);
            }
            for(auto i = newsLoad->RuTitle.begin(); i != newsLoad->RuTitle.end(); i++){
                news.push_back(*i);
            }
            for(auto i = newsLoad->RuContent.begin(); i != newsLoad->RuContent.end(); i++){
                news.push_back(*i);
            }

            news.push_back(0);
            numStage = 1;
        }else{
            char buff[2] = {'\0', '\0'};
            send(descriptor,buff,2,0);

            isEnd = true;
            LogRegister::writeLog("No have news");
            return;
        }
    }
    
    if(numStage == 1){
        int byte_write = 0;
        byte_write = send(descriptor,&(*(news.begin() + countPushByte)), news.size() - countPushByte,0);    
        countPushByte += byte_write;
        if(countPushByte == (int)news.size()){
            numStage = 2;
        }else{
            return;
        }
    }
    isEnd = true;
    LogRegister::writeLog("End get news");
}

void PacketGetCountNews::callUpdate(){//Send count news
    char num[sizeof(int)];
    int count = ManagerNews::getCountNews();
    std::memcpy(num, &count, sizeof(int));

    send(descriptor, num, sizeof(int), 0);
    isEnd = true;
    LogRegister::writeLog("End get count news");
}

void PacketRemoveNews::callUpdate(){//Admin: remove a news
    char buff[1];
    if(ManagerNews::removeNews(num) < 0){
        buff[0] = 1;
    }else{
        buff[0] = 0;
    }
    send(descriptor, buff, 1, 0);
    isEnd = true;
    LogRegister::writeLog("Remove news");
}

void loadNews(const int * descriptor,int *num,std::vector<std::list<char>> *strs){
    int byte_read = 0;
    char buf[1];
    while (0 < (byte_read = recv(*descriptor, buf, 1, 0)))
    {
        if(*num > 3 && buf[0] == '\0'){
            (*num)++;
            break;
        }else{
            (*strs)[*num].push_back(buf[0]);
            if(buf[0] == '\0'){
                (*num)++;
            }
        }
    }
}

void PacketAddNews::callUpdate(){
    loadNews(&descriptor, &num, &strs );
    if(num > 4){
        char buf[1];
        News news;
        news.EnTitle = strs[0];
        news.EnContent = strs[1];
        news.RuTitle = strs[2];
        news.RuContent = strs[3];

        if(ManagerNews::addNews(&news) < 0){
            buf[0] = 1;
        }else{
            buf[0] = 0;
        }
        send(descriptor, buf,1,0);
        isEnd = true;
        LogRegister::writeLog("End add news");
    }
}

void PacketEditNews::callUpdate(){
    loadNews(&descriptor, &numStr, &strs );
    if(numStr > 4){
        char buf[1];
        News news;
        news.EnTitle = strs[0];
        news.EnContent = strs[1];
        news.RuTitle = strs[2];
        news.RuContent = strs[3];

        if(ManagerNews::editNews(numNews,&news) < 0){
            buf[0] = 1;
        }else{
            buf[0] = 0;
        }
        send(descriptor, buf,1,0);
        isEnd = true;
        LogRegister::writeLog("End edit news");
    }
}

void PacketEndUpdateNews::callUpdate(){
    uint num = ManagerNews::getNumUpdate();
    char buf[sizeof(num)];
    std::memcpy(&buf, &num, sizeof(num));

    int countSend = send(descriptor, buf, sizeof(num),0);
    if(countSend <= 0){
        return;
    }else{
        isEnd = true;
        LogRegister::writeLog("The end to send update number news");
    }
}


//Textures

void PacketGetCountTexture::callUpdate(){//Send count textures
    char num[sizeof(int)];
    int count = ManagerTexture::getCountTexture();
    std::memcpy(num, &count, sizeof(int));

    send(descriptor, num, sizeof(int), 0);
    isEnd = true;
    LogRegister::writeLog("End get count textures");
}

void PacketRemoveTexture::callUpdate(){//Admin: remove a texture
    char buff[1];
    if(ManagerTexture::removeTexture(num) < 0){
        buff[0] = 1;
    }else{
        buff[0] = 0;
    }
    send(descriptor, buff, 1, 0);
    isEnd = true;
    LogRegister::writeLog("Remove texture");
}

void loadTexture(const int * descriptor, int *numStage, int * countReadByte, std::vector<char> *loadByte,Texture * texture){
    int read_byte = 0;
    char readNameC[1];
    if(*numStage == 0){
        while (true)
        {        
            read_byte = recv(*descriptor,readNameC, 1,0);
            if(read_byte > 0){
                if(readNameC[0] == '\0'){
                    (*numStage)++;
                    break;
                }else{
                    texture->nameTexture.append(&(readNameC[0]),1);
                }
            }else{
                break;
            }
        }
    }
    if(*numStage == 1){
        while (true)
        {        
            read_byte = recv(*descriptor,readNameC, 1,0);
            if(read_byte > 0){
                loadByte->push_back(readNameC[0]);
                (*countReadByte)++;
                if(*countReadByte == texture->countPixel * 4){
                    (*numStage)++;
                    break;
                }
            }else{
                break;
            }
        }
    }
    if(*numStage == 2){
        for(int i = 0;i < texture->countPixel;i++){
            texture->pixels[i].r = (*loadByte)[i * 4 + 0];
            texture->pixels[i].g = (*loadByte)[i * 4 + 1];
            texture->pixels[i].b = (*loadByte)[i * 4 + 2];
            texture->pixels[i].a = (*loadByte)[i * 4 + 3];
        }
        (*numStage)++;
    }
    return;
}

void PacketAddTexture::callUpdate(){
    loadTexture(&descriptor, &numStage, &countReadByte, &loadByte, &texture);
    if(numStage > 2){
        char buf[1];
        if(ManagerTexture::addTexture(&texture) < 0){
            buf[0] = 1;
        }else{
            buf[0] = 0;
        }
        send(descriptor, buf, 1, 0);
        isEnd = true;
        LogRegister::writeLog("End add texture");
    }   
}

void PacketEditTexture::callUpdate(){
    loadTexture(&descriptor, &numStage, &countReadByte, &loadByte, &texture);
    if(numStage > 2){
        char buf[1];
        if(ManagerTexture::editTexture(numTexture,&texture) < 0){
            buf[0] = 1;
        }else{
            buf[0] = 0;
        }
        send(descriptor, buf, 1, 0);
        isEnd = true;
        LogRegister::writeLog("End add texture");
    }   
}

void PacketGetTexture::callUpdate(){
    if(numStage == 0){
        texture = ManagerTexture::getTexture(numTexture);
        if(texture == NULL){
            char buff[2] = {'\0', '\0'};
            send(descriptor,buff,2,0);

            isEnd = true;
            numStage = -1;
            LogRegister::writeLog("End get texture");
            return;
        }else{
            bytes.reserve(texture->countPixel*4);
            for(int i = 0 ; i < texture->countPixel;i++){
                bytes[i * 4 + 0] = texture->pixels[i].r;
                bytes[i * 4 + 1] = texture->pixels[i].g;
                bytes[i * 4 + 2] = texture->pixels[i].b;
                bytes[i * 4 + 3] = texture->pixels[i].a;
            }
            numStage++;
        }
    }
    if(numStage == 1){
        int byteSend = 0;
        byteSend = send(descriptor, &(*(texture->nameTexture.begin() + countPushByte)),texture->nameTexture.length()+1-countPushByte,0);

        if(byteSend > 0){
            countPushByte += byteSend;
        }
        if(countPushByte >= (int)(texture->nameTexture.length())+1){
            numStage++;
            byteSend = 0;
        }
    }
    if(numStage == 2){
        int byteSend = 0;
        byteSend = send(descriptor, &(*(bytes.begin() + countPushByte)), bytes.size() - countPushByte, 0);
        if(byteSend > 0){
            countPushByte += byteSend;
        }
        if(countPushByte == (int)bytes.size()){
            numStage++;
            byteSend = 0;
        }
    }
    if(numStage == 3){
        isEnd = true;
        LogRegister::writeLog("End get texture");
    }
}

void PacketEndUpdateTexture::callUpdate(){
    uint num = ManagerTexture::getNumUpdate();
    char buf[sizeof(num)];
    std::memcpy(&buf, &num, sizeof(num));

    int countSend = send(descriptor, buf, sizeof(num),0);
    if(countSend <= 0){
        return;
    }else{
        isEnd = true;
        LogRegister::writeLog("The end to send update number texture");
    }
}

void PacketGetNameTexture::callUpdate(){
    if(numStage == 0){
        texture = ManagerTexture::getTexture(numTexture);
        if(texture == NULL){
            char buff[2] = {'\0', '\0'};
            send(descriptor,buff,2,0);
            
            numStage = -1;
            isEnd = true;
            LogRegister::writeLog("End get name texture");
            return;
        }else{
            numStage++;
        }
    }
    if(numStage == 1){
        int byteSend = 0;
        byteSend = send(descriptor, &(*(texture->nameTexture.begin() + countPushByte)),texture->nameTexture.length()+1-countPushByte,0);

        if(byteSend > 0){
            countPushByte += byteSend;
        }
        if(countPushByte >= (int)(texture->nameTexture.length())+1){
            numStage++;
            byteSend = 0;
        }
    }

    if(numStage == 2){
        isEnd = true;
        LogRegister::writeLog("End get name texture");
    }
}