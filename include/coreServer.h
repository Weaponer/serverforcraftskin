#include<list>
#include<sys/time.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h> 
#include<signal.h>
#include<fcntl.h>
#include<unistd.h>


#ifndef CORESERVER_H_INCLUDE
#define CORESERVER_H_INCLUDE

#define IP_SERVER  "194.58.119.53" 
#define PORT_SERVER 4246

enum ClientStatus{
    NoResolve = 0,
    StandartClient = 1,
    AdminClient = 2,
};

class BaseSocket{
public:
    static int startSocket(int countListen);

    static int closeSocket();
};

class UpdateSelect{

public:
    static int runUpdate();
};


class PacketWork{
public:
    bool isEnd;

    int descriptor;

    PacketWork(int descriptor){
        this->isEnd = false;
        this->descriptor = descriptor;
    }

    virtual void update() = 0;

    virtual ~PacketWork(){}
};

struct Client{
public:
    int descriptor;

    sockaddr_in addr;

    time_t timeArrivalMessage;

    ClientStatus status;

    PacketWork *paketWork;

    void destroyPacket();

    ~Client(){
        if(paketWork != NULL){
            delete paketWork;
        }
    }
};



class ConnectClient{
public:
    static int connectClient(int sock);

    static int disconnectClient(Client *);
};


class ServerSocketInfo{
public:
    static int baseSocketDescriptor;

    static std::list<Client*> clients;
};
#endif
