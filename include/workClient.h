#include"coreServer.h"
#include"fileNews.h"
#include"fileTexture.h"
#include<cstring>
#include<list>
#include<vector>

#ifndef WORKCLIENT_H_INCLUDE
#define WORKCLIENT_H_INCLUDE


#define MAX_TIME_DELAY 20

#define CODE_DISCONECT_CLIENT 100

#define GET_NEWS 1
#define SET_NEWS 2
#define EDIT_NEWS 3
#define REMOVE_NEWS 4
#define GET_COUNT_NEWS 5

#define GET_TEXTURE 6
#define SET_TEXTURE 7
#define EDIT_TEXTURE 8
#define REMOVE_TEXTURE 9
#define GET_COUNT_TEXTURE 10

//New functions
#define GET_END_UPDATE_NEWS 11
#define GET_END_UPDATE_TEXTURE 12
#define GET_NAME_TEXTURE 13

class ResolverClient{
public:
    static int resolveClient(Client *);

    static int checkTimeClient(Client *);

    static int sendDisconnectClint(Client *);
};


class PacketGetNews : public PacketWork{
private:
    std::vector<char> news;

    int numNews;

    int countPushByte;

    int numStage;
public: 

    PacketGetNews(int num, int descriptor) : PacketWork(descriptor) { 
        countPushByte = 0;
        numStage = 0;
        this->numNews = num;
    }

    void callUpdate();

    void update() override{
        callUpdate();
    }

    ~PacketGetNews() override {
    }
};

class PacketGetCountNews : public PacketWork{
public:
    PacketGetCountNews(int descriptor) : PacketWork(descriptor){}

    void callUpdate();

    void update() override{
        callUpdate();
    }

    ~PacketGetCountNews() override {
    }
};

class PacketRemoveNews : public PacketWork{
public:
    int num;
    PacketRemoveNews(int num, int descriptor) : PacketWork(descriptor){
        this->num = num;
    }

    void callUpdate();

    void update() override{
        callUpdate();
    }

    ~PacketRemoveNews() override {
    }
};


class PacketAddNews : public PacketWork{
private:
    std::vector<std::list<char>> strs;

    int num;
public:

    PacketAddNews(int descriptor) : PacketWork(descriptor){
        num = 0;
        strs.resize(4);
    }

    void callUpdate();

    void update() override{
        callUpdate();
    }

    ~PacketAddNews() override {
    }
};

class PacketEditNews : public PacketWork{
private:
    std::vector<std::list<char>> strs;

    int numStr;

    int numNews;
public:
    PacketEditNews(int num, int descriptor) : PacketWork(descriptor){
        numStr = 0;
        this->numNews = num;
        strs.resize(4);
    }

    void callUpdate();

    void update() override{
        callUpdate();
    }

    ~PacketEditNews() override {
    }
};

class PacketEndUpdateNews : public PacketWork{
public:
    PacketEndUpdateNews(int descriptor) : PacketWork(descriptor){

    }

    void callUpdate();

    void update() override{
        callUpdate();
    }

    ~PacketEndUpdateNews() override {
    }
};


//Packets for work from the textures
class PacketGetCountTexture : public PacketWork{
public:
    PacketGetCountTexture(int descriptor) : PacketWork(descriptor){}

    void callUpdate();

    void update() override{
        callUpdate();
    }

    ~PacketGetCountTexture() override {
    }
};

class PacketRemoveTexture : public PacketWork{
public:
    int num;
    PacketRemoveTexture(int num, int descriptor) : PacketWork(descriptor){
        this->num = num;
    }

    void callUpdate();

    void update() override{
        callUpdate();
    }

    ~PacketRemoveTexture() override {
    }
};

class PacketAddTexture : public PacketWork{
private:
    Texture texture = Texture(SIZE_TEXTURE,SIZE_TEXTURE);  

    std::vector<char> loadByte;

    int countReadByte;

    int numStage;
public:
    PacketAddTexture(int descriptor) : PacketWork(descriptor){
        numStage = 0;
        countReadByte = 0;
        loadByte.reserve(SIZE_TEXTURE*SIZE_TEXTURE*4);
    }

    void callUpdate();

    void update() override{
        callUpdate();
    }

    ~PacketAddTexture() override {
    }
};

class PacketEditTexture : public PacketWork{
private:
    Texture texture = Texture(SIZE_TEXTURE,SIZE_TEXTURE);  

    std::vector<char> loadByte;

    int countReadByte;

    int numStage;

    int numTexture;
public:
    PacketEditTexture(int numTexture,int descriptor) : PacketWork(descriptor){
        numStage = 0;
        countReadByte = 0;
        this->numTexture = numTexture;
        loadByte.reserve(SIZE_TEXTURE*SIZE_TEXTURE*4);
    }

    void callUpdate();

    void update() override{
        callUpdate();
    }

    ~PacketEditTexture() override {
    }
};

class PacketGetTexture : public PacketWork{
protected:
    Texture *texture;

    std::vector<char> bytes;

    int numTexture;

    int countPushByte;

    int numStage;
public: 

    PacketGetTexture(int num, int descriptor) : PacketWork(descriptor) { 
        countPushByte = 0;
        numStage = 0;
        this->numTexture = num;
    }

    void callUpdate();

    void update() override{
        callUpdate();
    }

    ~PacketGetTexture() override {
        if(texture != NULL){
            delete texture;
        }
    }
};

class PacketEndUpdateTexture : public PacketWork{
public:
    PacketEndUpdateTexture(int descriptor) : PacketWork(descriptor){

    }

    void callUpdate();

    void update() override{
        callUpdate();
    }

    ~PacketEndUpdateTexture() override {
    }
};

class PacketGetNameTexture : public PacketWork{
private:
    Texture *texture;

    int numTexture;

    int countPushByte;

    int numStage;
public: 

    PacketGetNameTexture(int num, int descriptor) : PacketWork(descriptor) { 
        countPushByte = 0;
        numStage = 0;
        this->numTexture = num;
    }

    void callUpdate();

    void update() override{
        callUpdate();
    }

    ~PacketGetNameTexture() override {
        if(texture != NULL){
            delete texture;
        }
    }
};
#endif