#include<iostream>
#include<sstream>
#include<string>
#include<algorithm>
#include<vector>
#include<list>

#include<sys/stat.h>
#include<string>
#include<ctime>
#include<stdio.h>
#include<unistd.h>


#ifndef FILENAWS_H_INCLUDE
#define FILENAWS_H_INCLUDE

#define NAME_DIRECTORY_NEWS "filesNews"
#define NAME_INFOFILE_NEWS "infData"

struct News{
public:
    std::list<char> EnTitle;
    std::list<char> EnContent;

    std::list<char> RuTitle;
    std::list<char> RuContent;
};

struct InfoFileNews{
public:
    FILE *file;

    uint endUpdateNum;

    int countNews;

    std::vector<std::string> vectorNews;

    void addEndUpdateNum(){
        if(endUpdateNum == UINT32_MAX){
            endUpdateNum = 0;
        }else{
            endUpdateNum++;
        }
    }
};

class ManagerNews{
private:
    static InfoFileNews infoFile;

    static int writeInfoFile();

    static int writeFileNews(FILE *, News *);
public:
    static int managerNews_Init();

    static int getCountNews();

    static int getNumUpdate();

    static News* getNews(int num);

    static int removeNews(int num);

    static int editNews(int num, News *);

    static int addNews(News *);

    static int managerNews_Close();
};
#endif