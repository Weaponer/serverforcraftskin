#include<stdio.h>
#include<sys/stat.h>
#include<ctime>
#include<string>
#include<algorithm>

#ifndef LOGREGISTER_H_INCLUDE
#define LOGREGISTER_H_INCLUDE

#define DERICTORY_NAME "logs"

#define IP_SERVER  "194.58.119.53" 
#define PORT_SERVER 4246

class LogRegister {
private:
    static FILE *writeFile;

public:
    static int initNewProcess();

    static int writeLog(const char *);

    static int writeWarning(const char *);

    static int writeError(const char *);

    static int closeProcess();
};

#endif