#include<iostream>
#include<sstream>
#include<string>
#include<algorithm>
#include<vector>

#include<sys/stat.h>
#include<string>
#include<ctime>
#include<stdio.h>
#include<unistd.h>


#ifndef FILETEXTURE_H_INCLUDE
#define FILETEXTURE_H_INCLUDE


#define NAME_DIRECTORY_TEXTURE "filesTexture"
#define NAME_INFOFILE_TEXTURE "filesTexture"

#define SIZE_TEXTURE 64

struct Pixel{
    char r;
    char g;
    char b;
    char a;
};

class Texture{
public:
    int countPixel;
    std::string nameTexture;
    Pixel *pixels;

    Texture(int x, int y){
        pixels = new Pixel[x*y];
        countPixel = x*y;
    }

    ~Texture(){
        delete[] pixels;
    }
};

class InfoTextures{
public:
    FILE *file;

    uint endUpdateNum;

    int countTextures;

    std::vector<std::string> vectorTextures;

    void addEndUpdateNum(){
        if(endUpdateNum == UINT32_MAX){
            endUpdateNum = 0;
        }else{
            endUpdateNum++;
        }
    }
};

class ManagerTexture{
private:
    static InfoTextures infoFile;

    static int writeInfoFile();

    static int writeFileTexture(FILE *, Texture *);
public:

    static int getCountTexture();

    static int getNumUpdate();

    static Texture* getTexture(int num);

    static int removeTexture(int num);

    static int editTexture(int num, Texture *);

    static int addTexture(Texture *);

    static int managerTexture_Close();

    static int managerTextureInit();
};
#endif